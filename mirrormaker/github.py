from github import Github, PaginatedList
from progress_passthrough.iterator_wrappers import wrap_source_len
from .common import build_description, build_website

# GitHub user authentication token
token = ''

# GitHub username (under this user namespace the mirrors will be created)
user = ''

target_forks = False


def iter_repos():
    """Finds all public GitHub repositories (which are not forks) of authenticated user.

    Returns:
     - Iterator (PyGithub PaginatedList) over public GitHub repositories.
    """

    gh = Github(token)

    # Return only public non forked repositories unless target_forks is set
    repos_iter = gh.get_user().get_repos(type="public")
    repos_w_len = wrap_source_len(repos_iter, length=repos_iter.totalCount)

    return repos_w_len.wrap(
        x for x in repos_w_len if not x.fork or target_forks
    )


def repo_exists(github_repos, repo_slug):
    """Checks if a repository with a given slug exists among the public GitHub repositories.

    Args:
     - github_repos: List of GitHub repositories.
     - repo_slug: Repository slug (usually in a form of path with a namespace, eg: "username/reponame").

    Returns:
     - True if repository exists, False otherwise.
    """

    return any(repo.full_name == repo_slug for repo in github_repos)


def get_repo_by_slug(github_repos, repo_slug):
    l = [repo for repo in github_repos if repo.full_name == repo_slug]
    return l[0] if len(l) > 0 else None


def create_repo(gitlab_repo):
    """Creates GitHub repository based on a metadata from given GitLab repository.

    Args:
     - gitlab_repo: GitLab repository which metadata (ie. name, description etc.) is used to create the GitHub repo.

    Returns:
     - JSON representation of created GitHub repo.
    """

    gh = Github(token)

    data = {
        'name': gitlab_repo.path,
        'description': build_description(gitlab_repo),
        'homepage': build_website(gitlab_repo),
        'private': False,
        'has_wiki': False,
        'has_projects': False
    }

    return gh.get_user().create_repo(**data)


def set_description(github_repo, description):
    github_repo.edit(description=description)


def set_website(github_repo, website):
    github_repo.edit(homepage=website)
