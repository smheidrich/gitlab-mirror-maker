from collections import ChainMap
from tqdm import tqdm as _tqdm
from progress_passthrough.tqdm import TqdmOnSource


class NicelyFormattedTqdm(_tqdm):
    def __init__(self, *args, **kwargs):
        kwargs = ChainMap(
            kwargs,
            dict(ncols=80, bar_format="{desc:14} {percentage:3.0f}% |{bar}|"),
        )
        super().__init__(*args, **kwargs)


class OurCustomTqdm(NicelyFormattedTqdm, TqdmOnSource):
    pass


tqdm = NicelyFormattedTqdm
tqdm_on_source = OurCustomTqdm
