from . import gitlab
from jinja2 import Template
description_template = (
    "{% if source_description %}{{source_description}} | {% endif %}"
    "mirror of {{source_url}}"
)
website_template = "{{source_url or ''}}"


def build_description(gitlab_repo):
    t = Template(description_template)
    s = dict(
        source_description=gitlab_repo.description,
        source_url=gitlab.get_project_url(gitlab_repo),
    )
    return t.render(s)


def build_website(gitlab_repo):
    t = Template(website_template)
    s = dict(
        source_url=gitlab.get_project_url(gitlab_repo),
    )
    return t.render(s)

