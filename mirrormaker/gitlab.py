from datetime import datetime
from dataclasses import dataclass
from dateutil.parser import isoparse
from gitlab import Gitlab

# GitLab user authentication token
token = ''


@dataclass
class GitLabPullMirror:
    # XXX most of this is not available for pull mirrors until this is fixed:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/354506
    # we "get the URL" (not really) by checking the import URL instead. AFAICT
    # you have to import before you can pull mirror, but no idea what happens
    # after you remove the pull mirror again - does the import URL get reset?
    url: str
    last_successful_update_at: datetime
    enabled: bool = True
    last_error = None


def iter_repos():
    """Finds all public GitLab repositories of authenticated user.

    Returns:
     - List of public GitLab repositories.
    """

    gl = Gitlab('https://gitlab.com/', private_token=token)

    repos_iter = gl.projects.list(visibility='public', owned=True,
        archived=False, as_list=False)

    return repos_iter


def get_user():
    gl = Gitlab('https://gitlab.com/', private_token=token)
    gl.auth()

    return gl.user


def get_project_url(gitlab_repo):
    return f'https://gitlab.com/{gitlab_repo.path_with_namespace}'


def get_repo_by_shorthand(shorthand):
    if "/" not in shorthand:
        user = get_user().username
        namespace, project = user, shorthand
    else:
        namespace, project = shorthand.rsplit("/", maxsplit=1)

    gl = Gitlab('https://gitlab.com/', private_token=token)

    project_id = "/".join([namespace, project])

    return gl.projects.get(project_id)


def get_push_mirrors(gitlab_repo):
    """Finds all configured mirrors of GitLab repository.

    Args:
     - gitlab_repo: GitLab repository.

    Returns:
     - List of mirrors.
    """

    return gitlab_repo.remote_mirrors.list()


def get_pull_mirror(gitlab_repo):
    """
    Gets a repo's pull mirror, if configured, or None.
    """
    # XXX see comments on GitLabPullMirror above
    import_url = gitlab_repo.import_url
    return GitLabPullMirror(
        url=import_url,
        last_successful_update_at=str(get_most_recent_commit_time(gitlab_repo))
    ) if import_url else None


def get_github_mirror(github_repos, mirrors):
    """Finds which, if any, of the given mirrors points to any of the public GitHub repositories.

    Args:
     - github_repos: List of GitHub repositories.
     - mirrors: List of mirrors configured for a single GitLab repository.

    Returns:
     - The mirror pointing to one of the GitHub repositories.
    """

    for mirror in mirrors:
        if not mirror.url:
            continue
        for repo in github_repos:
            if mirror.url.endswith(f'/{repo.full_name}.git'):
                return mirror

    return None


def create_mirror(gitlab_repo, github_user, github_token):
    """Creates a push mirror of GitLab repository.

    For more details see: 
    https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pushing-to-a-remote-repository-core

    Args:
     - gitlab_repo: GitLab repository to mirror.
     - github_token: GitHub authentication token.
     - github_user: GitHub username under whose namespace the mirror will be created (defaults to GitLab username if not provided).

    Returns:
     - JSON representation of created mirror.
    """

    # If github-user is not provided use the gitlab username
    if not github_user:
        github_user = gitlab_repo.owner["username"]

    data = {
        'url': f'https://{github_user}:{github_token}@github.com/{github_user}/{gitlab_repo.path}.git',
        'enabled': True
    }

    mirror = gitlab_repo.remote_mirrors.create(data)

    return mirror


def get_most_recent_commit(gitlab_repo):
    commits = gitlab_repo.commits.list()
    if commits and len(commits) > 0:
        return commits[0]


def get_most_recent_commit_time(gitlab_repo):
    most_recent_commit = get_most_recent_commit(gitlab_repo)
    if most_recent_commit:
        return isoparse(most_recent_commit.committed_date)
